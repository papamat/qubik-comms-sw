/*
 * watchdog.c
 *
 *  Created on: Mar 25, 2019
 *      Author: surligas
 */

#include "watchdog.h"
#include "error.h"

/**
 * Initializes the watchdog
 * @param w the watchdog structure
 * @param hiwdg the IWDG instance
 * @param mtx a valid mutex identifier
 * @param n the total number of sybsystems
 * @return 0 on success or negative error code
 */
int
watchdog_init(struct watchdog *w, IWDG_HandleTypeDef *hiwdg,
              osMutexId mtx, uint8_t n)
{
	if (!w || !hiwdg || !mtx || n > 32) {
		return -INVAL_PARAM;
	}

	w->registered = 0;
	w->subsystems = 0;
	w->subsystems_mask = 0;
	w->subsystems_num = n;
	for (uint8_t i = 0; i < n; i++) {
		w->subsystems_mask = (w->subsystems_mask << 1) | 0x1;
	}
	w->mtx = mtx;
	w->hiwdg = hiwdg;
	return NO_ERROR;
}

/**
 * Register a new subsystem. The registration is thread safe and internally
 * locks/unlocks the mutex provided at the \ref watchdog_init()
 * @param w the wathcdog structure
 * @param id pointer to store the ID of the subsystem
 * @return 0 on success or negative error code
 */
int
watchdog_register(struct watchdog *w, uint8_t *id)
{
	if (!w) {
		return -INVAL_PARAM;
	}
	osMutexWait(w->mtx, osWaitForever);
	if (!w || w->registered > w->subsystems_num) {
		osMutexRelease(w->mtx);
		return -INVAL_PARAM;
	}
	*id = w->registered;
	w->registered++;
	osMutexRelease(w->mtx);
	return NO_ERROR;
}

/**
 * Resets a specific subsystem
 * @param w the watchdog structure
 * @param id the ID of the subsystem acquired with \ref watchdog_register()
 * @return 0 on success or negative error code
 */
int
watchdog_reset_subsystem(struct watchdog *w, uint8_t id)
{
	if (!w || id > w->registered) {
		return -INVAL_PARAM;
	}
	w->subsystems |= 1 << id;
	return NO_ERROR;
}

/**
 * This function is responsible to reset the MCUs IWDG iff all of the registered
 * subsystems have already called at least once the
 * \ref watchdog_reset_subsystem(). If not, eventually the IWDG will be
 * triggered and the MCU will reset
 *
 * @param w the watchdog structure
 * @return 0 on success or negative error code.
 * @note: A 0 return value means that the call was successful not that the
 * IWDG was reset.
 */
int
watchdog_reset(struct watchdog *w)
{
	if (!w) {
		return -INVAL_PARAM;
	}

	if ((w->subsystems ^ w->subsystems_mask) == 0) {
		w->subsystems = 0;
		HAL_IWDG_Refresh(w->hiwdg);
	}
	return NO_ERROR;
}
