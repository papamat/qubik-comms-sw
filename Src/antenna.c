/**
 *  @file antenna.c
 *
 *  @date Jan 20, 2020
 *  @author drid
 *  @brief Antenna deployment control
 *
 *  @copyright Copyright (C) 2020 Libre Space Foundation (https://libre.space)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "antenna.h"
#include "power.h"
#include <stdlib.h>
#include "watchdog.h"

extern struct max17261_conf hmax17261;
extern struct watchdog hwdg;

/**
 * @brief Antenna deployment power control
 * Controls power (On/Off) to the antenna release mechanism
 * @param state ant_deploy_power_t value
 */
void
antenna_deploy_power(ant_deploy_power_t state)
{
	HAL_GPIO_WritePin(ANT_Deploy_GPIO_Port, ANT_Deploy_Pin, state);
}

/**
 * @brief Antenna deployment status
 * Provides information whether the antenna is deployed or stowed
 * @return ant_deploy_status_t value
 */
ant_deploy_status_t
antenna_deploy_status()
{
	return HAL_GPIO_ReadPin(ANT_Sense_GPIO_Port, ANT_Sense_Pin);
}

/**
 * @brief Tests antenna deploy mechanism
 *
 * Performs a deploy mechanisms test by applying power to the deploy mechanism for a short
 * period and measuring current drawn an voltage level.
 *
 * This will provide information regarding any problems in the control mechanism as well as
 * information about battery capability for full deployment.
 *
 * Voltage and current values are stored in power_status structure
 *
 *
 * @param power_status power status structure
 * @return Test result
 */
ant_deploy_test_status_t
antenna_deploy_test(struct power_status *power_status)
{
	int16_t start_current;

	start_current = max17261_get_current(&hmax17261);

	antenna_deploy_power(ANT_DEPLOY_ON);
	HAL_Delay(ANT_DEPLOY_TEST_TIME);
	power_status->ant_deploy_test_current = max17261_get_current(
	                &hmax17261) - start_current;
	power_status->ant_deploy_test_voltage = max17261_get_voltage(&hmax17261);
	antenna_deploy_power(ANT_DEPLOY_OFF);

	if (abs(power_status->ant_deploy_test_current) < (ANT_DEPLOY_TEST_CURRENT / 2))
		return ANT_DEPLOY_TEST_FAIL;
	if (abs(power_status->ant_deploy_test_current) < ANT_DEPLOY_TEST_CURRENT)
		return ANT_DEPLOY_TEST_DEGRADED;
	return ANT_DEPLOY_TEST_OK;
}

/**
 * @brief Antenna deploy sequence activation
 * Starts the antenna deploy activation sequence. Current is applied on the deploy resistors until
 * either antenna deploy status is changed or timeout occurs
 *
 * @return Time in seconds for the antenna to deploy
 */
uint8_t
antenna_deploy()
{
	uint32_t time;
	uint16_t cntr = 0;
	uint8_t wdgid = 0, ant_status_fail = 0;

	ant_deploy_status_t status = antenna_deploy_status();
	ant_status_fail = (status == ANT_DEPLOYED);

	time = HAL_GetTick();
	antenna_deploy_power(ANT_DEPLOY_ON);
	while (((status == ANT_STOWED) | ant_status_fail)
	       && (cntr++ < (ANT_DEPLOY_TIME * 10))) {
		HAL_Delay(100);
		status = antenna_deploy_status();
		watchdog_reset_subsystem(&hwdg, wdgid);
	}
	HAL_Delay(ANTENNA_POST_DEPLOY_DELAY);
	antenna_deploy_power(ANT_DEPLOY_OFF);
	return (HAL_GetTick() - time - ANTENNA_POST_DEPLOY_DELAY) / 1000;
}
