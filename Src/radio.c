
#include <radio.h>
#include <string.h>
#include <FreeRTOS.h>
#include <queue.h>

extern QueueHandle_t rx_queue;

void
frame_received(const uint8_t *pdu, size_t len)
{
	if (!pdu || !rx_queue || len > MAX_RX_FRAME_LEN) {
		return;
	}
	struct rx_frame p;
	p.len = len;
	memcpy(p.pdu, pdu, len);
	BaseType_t high_prio_woken = pdFALSE;
	xQueueSendFromISR(rx_queue, &p, &high_prio_woken);
}

void
tx_complete()
{

}
