/*
 *  @file:  max17261_driver.c
 *  @brief: MAX17261 Driver I2C communication functions
 *
 */

#include "max17261_driver.h"
#include "main.h"
#include "cmsis_os.h"

extern I2C_HandleTypeDef hi2c2;

/**
 * @brief I2C Read function
 * Reads 16bit data from device
 * @param reg Register to read from
 * @param value Pointer to write value
 * @return 0 on success error code otherwise
 */
int
max17261_read_word(uint8_t reg, uint16_t *value)
{
	HAL_StatusTypeDef status = HAL_OK;
	uint8_t buf[2];

	status = HAL_I2C_IsDeviceReady(&hi2c2, (uint16_t)MAX17261_ADDRESS, 1,
	                               1000);
	if (status != HAL_OK) {
		return status;
	}

	status = HAL_I2C_Mem_Read(&hi2c2, (uint8_t)MAX17261_ADDRESS,
	                          (uint8_t) reg, I2C_MEMADD_SIZE_8BIT, buf, 2, 1000);

	if (status != HAL_OK) {
		return status;
	}
	*value = (uint16_t)((buf[1] << 8) | buf[0]);
	return status;

}

/**
 * @brief I2C Write function
 * Writes 16bit data to device
 * @param reg Register to write to
 * @param value Value to write
 * @return 0 on success error code otherwise
 */
int
max17261_write_word(uint8_t reg, uint16_t value)
{
	HAL_StatusTypeDef status = HAL_OK;
	status = HAL_I2C_IsDeviceReady(&hi2c2, (uint16_t)MAX17261_ADDRESS, 1,
	                               1000);
	if (status
	    != HAL_OK) {
		return status;
	}

	status = HAL_I2C_Mem_Write(&hi2c2, (uint8_t)MAX17261_ADDRESS,
	                           (uint8_t) reg, I2C_MEMADD_SIZE_8BIT, value,
	                           2, 1000);

	if (status != HAL_OK) {
		return status;
	}
	return status;
}

/**
 * @brief I2C Write with verify function
 * Writes 16bit data to device and verifies content
 * @param reg Register to write to
 * @param value Value to write
 * @return 0 on success error code otherwise
 */
int
max17261_write_verify(uint8_t reg, uint16_t value)
{
	uint8_t wcount = 0, ret;
	uint16_t readback;
	do {
		if ((ret = max17261_write_word(reg, value)) != HAL_OK)
			return ret;
		HAL_Delay(1);
		if ((ret = max17261_read_word(reg, &readback)) != HAL_OK)
			return ret;
	} while (readback != value && wcount++ < MAX17261_RB_THRESHOLD);
	return (wcount == MAX17261_RB_THRESHOLD ? 0 : 0x04);
}

