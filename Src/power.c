/*
 *  @file power.c
 *
 *  @date Jan 16, 2020
 *  @author drid
 *  @brief Power status acquisition
 *
 *  @copyright Copyright (C) 2020 Libre Space Foundation (https://libre.space)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "power.h"
//#include "max17261_driver.h"
extern struct max17261_conf hmax17261;
extern ADC_HandleTypeDef hadc1;

/**
 * @brief Update power status structure from max17261 values
 * @param power_status
 */
void
update_power_status(struct power_status *power_status)
{
	// Get voltage from alternate source
	HAL_ADC_Start(&hadc1);
	if (HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED) !=  HAL_OK) {
		Error_Handler();
	}

	if (HAL_ADC_Start(&hadc1) != HAL_OK) {
		Error_Handler();
	}

	if (HAL_ADC_PollForConversion(&hadc1, 10) != HAL_OK) {
		Error_Handler();
	}

	if ((HAL_ADC_GetState(&hadc1) & HAL_ADC_STATE_REG_EOC) ==
	    HAL_ADC_STATE_REG_EOC) {
		power_status->voltage_alt = HAL_ADC_GetValue(&hadc1) * ADC_VOLTAGE_COEFFICIENT;
	}

	// Get power values from MAX17261
	max17261_init(&hmax17261);
	power_status->voltage = max17261_get_voltage(&hmax17261);
	power_status->voltage_avg = max17261_get_average_voltage(&hmax17261);
	power_status->current = max17261_get_current(&hmax17261);
	power_status->current_avg = max17261_get_average_current(&hmax17261);
	power_status->SOC = max17261_get_SOC(&hmax17261);
	power_status->temperature = max17261_get_temperature(&hmax17261);
	power_status->temperature_die = max17261_get_die_temperature(&hmax17261);
	power_status->temperature_avg = max17261_get_average_temperature(&hmax17261);
	power_status->TTE = max17261_get_TTE(&hmax17261);

	max17261_get_minmax_voltage(&hmax17261, &power_status->voltage_min,
	                            &power_status->voltage_max);
	max17261_get_minmax_current(&hmax17261, &power_status->current_min,
	                            &power_status->current_max);
}


