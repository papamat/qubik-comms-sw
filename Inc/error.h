

#ifndef ERROR_H_
#define ERROR_H_

/**
 * Qubik COMMS error codes
 */
enum {
	NO_ERROR,   //!< All ok
	INVAL_PARAM,//!< Invalid parameter
};

#endif /* ERROR_H_ */
