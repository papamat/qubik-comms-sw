#ifndef TEST_H_
#define TEST_H_

/******************************************************************************
 ***************************** IMPORTANT NOTE *********************************
 ******************************************************************************
 * For safety reasons, (testing RX without attenuation etc) all tests are
 * disabled by default. Enable only at your local copy of the project and
 * do not commit any change on this file that has an enabled test. By default
 * all tests should be disabled and under no circumstances the satellite should
 * transmit anything by default.
 */
#define TEST_RX         0
#define TEST_CW         0
#define TEST_FSK_AX25   0

int
test_task();

#endif /* TEST_H_ */
