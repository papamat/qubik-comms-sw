/*
 * max17261_driver.h
 *
 *  Created on: Nov 18, 2019
 *      Author: drid
 *
 *
 *  @file:  max17261_driver.h
 *  @brief: MAX17261 Driver setup
 *
 */

#ifndef INC_MAX17261_DRIVER_H_
#define INC_MAX17261_DRIVER_H_

#include <max17261.h>


#define	BATTERY_CAPACITY	2600
#define BATTERY_CRG_TERM_I	0x140
#define BATTERY_V_EMPTY		3300 / 10
#define BATTERY_V_Recovery	3880 / 40

int
max17261_read_word(uint8_t reg, uint16_t *value);

int
max17261_write_word(uint8_t reg, uint16_t value);

int
max17261_write_verify(uint8_t reg, uint16_t value);

#endif /* INC_MAX17261_DRIVER_H_ */
